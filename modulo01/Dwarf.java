
public class Dwarf{
    private String nome;
    private float qtdvida = 110.0f;
    
    public Dwarf(String nome){
        this.nome = nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }    
    
    public void setQtdVida(float qtdvida){
        this.qtdvida = qtdvida;
    } 
    
    public String getNome(){
        return this.nome;
    }
    
    public float getQtdVida(){
        return this.qtdvida;
    }
}
