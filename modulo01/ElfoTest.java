

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    @Test
    public void atirarFlechaReduzirFlechaAumentarXP(){
        Elfo personagem = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf ("Anaozinho");
        personagem.atirarFlechaDwarf(novoAnao);
        personagem.atirarFlechaDwarf(novoAnao);
        personagem.atirarFlechaDwarf(novoAnao);
        personagem.atirarFlechaDwarf(novoAnao);
        personagem.atirarFlechaDwarf(novoAnao);
        assertEquals(70.0,novoAnao.getQtdVida(),0.1f);
        assertEquals(0,personagem.getQuantidadeFlecha());
    
    }
    
}
