
public class Elfo{
    
    private String nome;
    private int experiencia = 0;
    private Item flecha = new Item (4, "Flecha");
    private Item arco = new Item (1, "Arco");
    
    
    public Elfo(String nome){
        this.nome = nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }    
    
    public String getNome(){
        return this.nome;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public Item getFlecha(){
        return this.flecha;
        
    }
    
    public int getQuantidadeFlecha(){
        return this.flecha.getQuantidade();
        
    }
    
    public void aumentarXP(){
        experiencia++;
    }
    
    public void atirarFlecha(){
        int qtdAtual = flecha.getQuantidade();
       
        if (qtdAtual > 0){
            flecha.setQuantidade(qtdAtual - 1);
            this.aumentarXP();
        }
    }
    
    public void atirarFlechaDwarf(Dwarf novoAnao){
       
        int qtdAtual = flecha.getQuantidade();
        float qtdVidaAnao = novoAnao.getQtdVida();
        
        
        if (qtdAtual > 0){
            flecha.setQuantidade(qtdAtual - 1);
            novoAnao.setQtdVida(qtdVidaAnao - 10.0f);
            this.aumentarXP();
        }
    }
}
